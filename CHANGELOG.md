## 1.0.3
-   Added docker-compose to build a MySQL container
-   Added `/users` routing to the application to query the DB
-   Added a stage in the pipeline to push the documentation do DockerHub
-   Updated the README
-   Updated the Makefile
-   Updated folder structure
-   Updated unittest
-   Changelog automatically added into the README just before being pushed to DockerHub
!!! FROM THIS VERSION THIS IMAGE REQUIRES A DB. PLEASE CHECK THE GITLAB REPO !!!

## 1.0.2
-   Adding unittest
-   Converting routing /user to /hello

## 1.0.1
-   Adding new routing

## 1.0.0
-   First version
