from flask import Flask
import mysql.connector

app = Flask(__name__)


def db_connect():
    return mysql.connector.connect(
            user='myuser',
            host='db',
            port='3306',
            password='password',
            database='mydb')


def exec_query(db, query):
    cursor = db.cursor()
    cursor.execute(query)
    result = cursor.fetchall()
    cursor.close()
    return result


@app.route("/")
def hello():
    return "Index Page!"


@app.route('/hello/<username>')
def profile(username):
    return "Hello {}!".format(username.capitalize())


@app.route("/users")
def users():
    db = db_connect()
    user_list = ''
    try:
        query = "SELECT * FROM users"
        result = exec_query(db, query)
        for user in result:
            user_list += f"{user[1]}\n"
    except Exception as e:
        return str(e)
    finally:
        db.close()

    return user_list
