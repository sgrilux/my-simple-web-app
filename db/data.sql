#CREATE DATABASE IF NOT EXISTS mydb DEFAULT CHARACTER SET utf8;
#CREATE USER IF NOT EXISTS 'myuser'@'%' IDENTIFIED BY 'password';
#GRANT ALL PRIVILEGES ON mydb.* TO 'myuser'@'%';
#FLUSH PRIVILEGES;

CREATE TABLE IF NOT EXISTS mydb.users (
	id INT NOT NULL AUTO_INCREMENT,
	user VARCHAR(20) NOT NULL,
	PRIMARY KEY (id)
);

INSERT INTO mydb.users(user) VALUES ('user1');
INSERT INTO mydb.users(user) VALUES ('user2');
INSERT INTO mydb.users(user) VALUES ('user3');
INSERT INTO mydb.users(user) VALUES ('user4');
INSERT INTO mydb.users(user) VALUES ('user5');
