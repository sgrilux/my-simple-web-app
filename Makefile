NAME   := sgrilux/my-simple-web-app
TAG    := $$(cat VERSION)
IMG    := ${NAME}:${TAG}
LATEST := ${NAME}:latest
 
REQS_TEST=requirements_test.txt

.PHONY: clean clean-pyc lint

help:
	@echo 'Usage:'
	@echo '     make help            Show this message'
	@echo '     make install-test    Install dependencies required for tests'
	@echo '     make clean           Remove all build, test and Python artifacts as well as docker-compose images'
	@echo '     make lint            Check style with flake8'
	@echo '     make test            Run tests with pytest'
	@echo '     make docker-compose  Run docker compose to build the web and db images'
	@echo ''

install_test:
	test -f ${REQS_TEST} && pip3 install -Ur ${REQS_TEST}

build:
	@docker build -t ${IMG} web/
	@docker tag ${IMG} ${LATEST}
 
push:
	@docker push ${IMG}
	@docker push ${NAME}

clean: clean-pyc docker-compose-clean

clean-pyc:
	find . -name '*.pyc' -exec rm -f {} +
	find . -name '*.pyo' -exec rm -f {} +

docker-compose-clean:
	@docker rmi my-simple-web-app_web my-simple-web-app_db

docker-system-clean:
	@docker system prune -f

lint: install_test
	@flake8 web/index.py

test: install_test
	python3 -m unittest discover -s tests -v

docker-compose: 
	@docker-compose up
