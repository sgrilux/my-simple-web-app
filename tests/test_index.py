import os
import unittest
from unittest.mock import patch
from web.index import app, users

class TestMyApp(unittest.TestCase):

    def setUp(self):
        self.app = app.test_client()
        self.app.testing = True 

    def test_root(self):
        result = self.app.get('/')
        self.assertEqual(result.status_code, 200)
        self.assertIn(b"Index Page!", result.data)

    def test_hello_user(self):
        result = self.app.get('/hello/sgrilux')
        self.assertEqual(result.status_code, 200)
        self.assertIn(b"Hello Sgrilux!", result.data)

    @patch('web.index.exec_query')
    @patch('web.index.db_connect')
    def test_users(self, db_connect, mock_exec_query):
        fake_result = [(1,'user1'),
                       (2,'user2'),
                       (3,'user3'),
                       (4,'user4'),
                       (5,'user5')]
        user_list = "user1\nuser2\nuser3\nuser4\nuser5\n"
        mock_exec_query.return_value = fake_result
        ret_user = users()
        self.assertEqual(ret_user, user_list)
