# My Simple Web Application

**!!! THIS IS NOT INTENDED TO BE A REAL LIFE SCENARIO TO BE USED IN PRODUCTION.
IT IS JUST AN EXERCISE TO SHOW HOW TO BUILD A SIMPLE APPLICATION WITH DOCKER !!!**

The scope of this repository may change during the course of time, so please always
check the CHANGELOG to see the updates.

Gitlab CI automatically build the image and push to DockerHub with the new TAG

## Usage

```bash
$ git clone git@gitlab.com:sgrilux/my-simple-web-app.git
$ cd my-simple-web-app
$ make docker-compose
$ docker-compose -d up
```

Connect with your browser to 

-   `http://<IP>:8080/`
-   `http://<IP>:8080/hello/<username>`
-   `http://<IP>:8080/users`

### Stop

$ docker-compose down

### Clean up

```bash
$ make clean
```

### Linting & Test

```bash
$ make lint
```

```bash
$ make test
```

## Links

-   [Docker Hub Repository](https://cloud.docker.com/repository/docker/sgrilux/my-simple-web-app)
-   [Gitlab Repository](https://gitlab.com/sgrilux/my-simple-web-app/tags)

## External repository

To update the Docker Hub repository I've used the [readme-to-dockerhub](https://github.com/SISheogorath/readme-to-dockerhub)

## CHANGELOG
